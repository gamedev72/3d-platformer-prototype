# 3D Platformer prototype

This is a project done for "Unity Learn Junior Programmer" course, on learn.unity.com

Link to play: https://play.unity.com/mg/other/webgl-builds-251508

The main objective was to use Object-Oriented Programming and its four pillars (encapsulation, polymorphism, abstraction and inheritance), in a Unity project.
I've done that implementing a "Enemy" class and many child classes.
You can view this in scripts in "Assets" > "Script" folder.

For the player, i've used the "3D Platform Prototype" Assets, unlocked in the course.

Done with Unity 2021.3.8f1.

##Known Issues
- Keyboard controls work, but cannot control camera. I have to figure out how the input works in Unity
- HP and enemy damage to player don't work, i have to implement them yet
- Coins, name and score are not saved anywhere
