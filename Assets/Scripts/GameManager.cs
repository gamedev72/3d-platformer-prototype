using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    //ENCAPSULATION
    public static GameManager Instance { get; private set; }

    [SerializeField] private string m_PlayerName;

    //ENCAPSULATION
    public string PlayerName
    {
        get { return m_PlayerName; }
        set
        {
            if (value.Length <= 0)
            {
                Debug.LogError("Please insert a name");
            }
            else if (value.Length > 20)
            {
                Debug.LogError("This name is too long, max 20 characters");
            }
            else
            {
                m_PlayerName = value;
            }
        }
    }

    private int m_Money;
    public int Money
    {
        get { return m_Money; }
        set
        {
            if (value < 0)
            {
                m_Money = 0;
            }
            else
            {
                m_Money = value;
            }
        }
    }

    public int Score;

    public int PlayerHP;

    private void GetPlayerHP()
    {
        if(SceneManager.GetActiveScene().buildIndex == 1)
        {
            PlayerHP = GameObject.Find("Player").GetComponent<PlayerController>().HealthPoints;
        }
    }

    // Start is called before the first frame update
    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    private void Update()
    {
        //ABSTRACTION
        GetPlayerHP();
    }
}
