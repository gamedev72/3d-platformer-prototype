using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //ENCAPSULATION
    public int JumpDamage { get; private set;} = 1;

    [SerializeField] private int m_HealthPoints = 5;
    public int HealthPoints
    {
        get { return m_HealthPoints; }
        set
        {
            if (value < 0)
            {
                m_HealthPoints = 0;
            }
            else
            {
                m_HealthPoints = value;
            }
        }
    }

    private void PlayerDeath()
    {
        Destroy(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(HealthPoints == 0)
        {
            //ABSTRACTION
            PlayerDeath();
        }
    }
}
