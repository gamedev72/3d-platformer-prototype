using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicEnemy : Enemy
{
    //POLYMORPHISM
    public override void Move()
    {
        throw new System.NotImplementedException();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            EnemyHit(player.JumpDamage);
        }
    }

    private void Awake()
    {
        HealthPoints = 1;
        ScoreValue = 100;
        DamagePoints = 1;
    }

    private void Update()
    {
        if (HealthPoints == 0)
        {
            EnemyDeath();
        }
    }
}
