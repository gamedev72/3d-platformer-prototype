using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FlyingEnemy : Enemy
{
    [SerializeField] protected float speed = 1.0f;
    private bool isRotated = false;

    /*kind of same as moving enemy, except for move function*/
    //POLYMORPHISM
    public override void Move()
    {
        transform.Translate(0, speed * Time.deltaTime, 0);

        if (Mathf.RoundToInt(Time.realtimeSinceStartup) % 2 == 0)
        {
            if (isRotated == false)
            {
                Vector3 rotation = transform.localEulerAngles;
                rotation.y += 180;
                transform.localEulerAngles = rotation;
            }
            isRotated = true;
        }
        else
        {
            isRotated = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            EnemyHit(player.JumpDamage);
        }
    }

    private void Awake()
    {
        HealthPoints = 1;
        ScoreValue = 150;
        DamagePoints = 1;
    }

    private void Update()
    {
        
        Move();

        if (HealthPoints == 0)
        {
            EnemyDeath();
        }
    }
}
