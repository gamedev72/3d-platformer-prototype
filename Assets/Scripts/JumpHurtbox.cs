using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpHurtbox : MonoBehaviour
{
    public static PlayerController playerControllerScript;
    public Enemy parent { get; private set; }

    // Start is called before the first frame update
    void Start()
    {
        playerControllerScript = GameObject.Find("Player").GetComponent<PlayerController>();
        parent = transform.parent.GetComponent<Enemy>();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
