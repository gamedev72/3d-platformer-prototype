using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpingEnemy : Enemy
{
    private bool isOnGround = false;
    [SerializeField] private int jumpHeight;
    private Rigidbody enemyRb;

    //POLYMORPHISM
    public override void Move()
    {
        if (isOnGround && Mathf.RoundToInt(Time.realtimeSinceStartup) % 2 == 0)
        {
            enemyRb.AddForce(Vector3.up * jumpHeight, ForceMode.Impulse);

            isOnGround = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            EnemyHit(player.JumpDamage);
        }
    }

    protected override void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isOnGround = true;
        }
    }

    private void Awake()
    {
        HealthPoints = 1;
        ScoreValue = 200;
        DamagePoints = 1;

        enemyRb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        Move();

        if(HealthPoints == 0)
        {
            EnemyDeath();
        }
    }
}
