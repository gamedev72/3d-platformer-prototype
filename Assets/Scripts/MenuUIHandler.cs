using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class MenuUIHandler : MonoBehaviour
{
    [SerializeField] private TMP_InputField nameField;
    [SerializeField] private TMP_Text NameText;
    [SerializeField] private TMP_Text HPText;
    [SerializeField] private TMP_Text ScoreText;
    [SerializeField] private TMP_Text MoneyText;

    public void StartGame()
    {
        GameManager.Instance.PlayerName = nameField.text;
        SceneManager.LoadScene(1);
    }

    public void ReturnToMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void QuitGame()
    {
#if UNITY_EDITOR
        EditorApplication.ExitPlaymode();
#else
        Application.Quit();
#endif
    }

    private void Update()
    {
        if(SceneManager.GetActiveScene().buildIndex == 1)
        {
            if (GameManager.Instance.PlayerName != "") //for debugging purposes
            {
                NameText.text = GameManager.Instance.PlayerName;
            }
            HPText.text = "HP: " + GameManager.Instance.PlayerHP;
            MoneyText.text = "Money: " + GameManager.Instance.Money;
            ScoreText.text = "Score: " + GameManager.Instance.Score;
        }
    }
}
