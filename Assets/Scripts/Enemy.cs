using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy : MonoBehaviour
{
    [SerializeField] protected static PlayerController player;

    protected int HealthPoints;
    protected int DamagePoints;
    protected int ScoreValue;
    protected GameObject ItemToDrop;

    //POLYMORPHISM
    public abstract void Move();
    public virtual void AddScore()
    {
        GameManager.Instance.Score += ScoreValue;
    }
    public virtual void DropItem()
    {
        if (ItemToDrop != null)
        {
            Instantiate(ItemToDrop, transform.position, transform.rotation);
        }
    }

    public virtual void DamagePlayer()
    {
        player.GetComponent<PlayerController>().HealthPoints -= DamagePoints;
    }

    public virtual void EnemyHit(int playerDamage)
    {
        HealthPoints -= playerDamage;
    }

    public virtual void EnemyDeath()
    {
        AddScore();
        DropItem();
        Destroy(gameObject);
    }

    protected virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.Equals(player))
        {
            DamagePlayer();
        }
    }

    private void Start()
    {
        player = GameObject.Find("Player").GetComponent<PlayerController>();
    }
}
